package states;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.automata.transducers.impl.MealyTransition;
import net.automatalib.commons.util.Triple;
import net.automatalib.words.Alphabet;

import java.util.*;
import java.util.stream.Collectors;


/**
 * {@code StateHandler} provides a bunch of operations to handle states in a Mealy Machine.
 *
 * @author Mirela Mileva
 */
public class StateHandler {

    private static void findAllPaths(FastMealyState start, FastMealyState target, List<Triple> current, List<List<Triple>> results, FastMealy mealy) {

        if (start.getId() == target.getId()) {
            results.add(new ArrayList<>(current));
            //check if the parameter target is actually a target state in the mealy, if so add its eventually given selfloop transitions to the current path
            if (isTarget(target, mealy)) {
                for (TransDescr transDescr : getSuccTransitions(target, mealy)) {
                    Triple edge = Triple.of(start, transDescr.getState(), transDescr.input);
                    current.add(edge);
                    results.add(new ArrayList<>(current));
                    current.remove(edge);
                }
            }
        } else {
            List<TransDescr> transDescrs = getSuccTransitions(start, mealy);
            for (TransDescr next : transDescrs) {
                Triple edge = Triple.of(start, next.getState(), next.input);
                if (!current.contains(edge)) {
                    current.add(edge);
                    findAllPaths((FastMealyState) edge.getSecond(), target, current, results, mealy);
                    current.remove(current.size() - 1);
                }
            }
        }
    }

    /**
     * Calculating all paths between two states, loops are added once.
     *
     * @param start  The initial state in mealy.
     * @param target The target state.
     */
    public static List<List<String>> findAllPaths(FastMealyState start, FastMealyState target, FastMealy mealy) {
        List<List<Triple>> result = new ArrayList<>();
        List<Triple> running = new ArrayList<>();
        running.add(Triple.of(null, start, null));
        findAllPaths(start, target, running, result, mealy);

        List<List<String>> pathWords = new ArrayList<>();

        //get only the inputs read on the paths
        for (List<Triple> edgePath : result) {
            List<String> path = edgePath.stream().map(e -> (String) e.getThird()).collect(Collectors.toList());
            List<String> words = new ArrayList<>();

            for (String s : path) {
                if (s != null)
                    words.add(s);
            }

            pathWords.add(words);
        }
        return pathWords;
    }

    /**
     * @return All subsequent transitions of a given state.
     */
    public static ArrayList<TransDescr> getSuccTransitions(FastMealyState state, FastMealy mealy) {
        ArrayList<TransDescr> succTransitions = new ArrayList<>();

        for (FastMealyState currentState : (Collection<FastMealyState>) mealy.getStates()) {
            for (String alph : (Alphabet<String>) mealy.getInputAlphabet()) {

                for (MealyTransition transition : (Collection<MealyTransition>) mealy.getTransitions(state, alph)) {
                    FastMealyState succ = (FastMealyState) transition.getSuccessor();
                    if (succ.getId() == currentState.getId()) {
                        TransDescr succTransition = new TransDescr(transition, alph, currentState);
                        succTransitions.add(succTransition);
                    }
                }
            }
        }
        return succTransitions;
    }

    /**
     * @return All preceding transitions of a given state.
     */
    public static ArrayList<TransDescr> getPredTransitions(FastMealyState state, FastMealy mealy) {
        ArrayList<TransDescr> predTransitions = new ArrayList<>();

        if (state.equals(mealy.getInitialState())) {
            return predTransitions;
        }

        for (FastMealyState currState : (Collection<FastMealyState>) mealy.getStates()) {
            for (String alph : (Alphabet<String>) mealy.getInputAlphabet()) {

                for (MealyTransition transition : (Collection<MealyTransition>) mealy.getTransitions(currState, alph)) {
                    if (transition != null) {
                        FastMealyState succ = (FastMealyState) transition.getSuccessor();
                        if (succ.getId() == state.getId()) {
                            TransDescr predTransition = new TransDescr(transition, alph, currState);
                            predTransitions.add(predTransition);
                        }
                    }
                }
            }
        }
        return predTransitions;
    }

    /**
     * @return All subsequent states of a given state.
     */
    public static ArrayList<FastMealyState> getSuccStates(FastMealyState state, FastMealy mealy) {

        ArrayList<FastMealyState> succStates = new ArrayList<>();
        ArrayList<TransDescr> succTransitions = getSuccTransitions(state, mealy);
        for (TransDescr transDescr : succTransitions) {
            succStates.add((FastMealyState) transDescr.state);
        }

        return succStates;
    }

    /**
     * @return All preceding states of a given state.
     */
    public static ArrayList<FastMealyState> getPredStates(FastMealyState state, FastMealy mealy) {

        ArrayList<FastMealyState> predStates = new ArrayList<>();
        ArrayList<TransDescr> predTransitions = getPredTransitions(state, mealy);
        for (TransDescr transDescr : predTransitions) {
            predStates.add((FastMealyState) transDescr.state);
        }

        return predStates;
    }


    /**
     * @return True, if the state is a sink state, i.e. has no outgoing transitions, otherwise false.
     */
    public static boolean isFail(FastMealyState state, FastMealy mealy) {

        return getSuccTransitions(state, mealy).size() == 0;

    }

    /**
     * @return True, if the state is a target state, i.e. has outgoing transitions possibly only to itself, otherwise false.
     */
    private static boolean isTarget(FastMealyState state, FastMealy mealy) {

        Set<FastMealyState> succStatesWithoutDuplicates = new HashSet<>(getSuccStates(state,mealy));

        return succStatesWithoutDuplicates.isEmpty() || (succStatesWithoutDuplicates.size() == 1 && succStatesWithoutDuplicates.contains(state));
    }
}
