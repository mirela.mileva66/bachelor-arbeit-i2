package states;

import net.automatalib.automata.transducers.impl.MealyTransition;

import java.util.Objects;

/**
 * The class holds a representation of a transition, consisting of the transition itself, the read input and either a preceding or a subsequent state.
 *
 * @param <I> The input class.
 * @param <S> The state class.
 */
public class TransDescr<I, S> {

    private final MealyTransition transition;
    final I input;
    final S state;

    public TransDescr(MealyTransition transition, I input, S state) {
        this.transition = transition;
        this.input = input;
        this.state = state;
    }

    //transDescr holds the transition, the input and either the predecessor or the successor state
    public MealyTransition getTransition() {
        return transition;
    }

    public I getInput() {
        return input;
    }

    public S getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransDescr<?, ?> that = (TransDescr<?, ?>) o;
        return Objects.equals(transition, that.transition) &&
                Objects.equals(input, that.input) &&
                Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transition, input, state);
    }
}
