package minimisation;

import com.google.common.collect.Maps;
import equivalence.ComputedFunctionEquivalence;
import equivalence.HistoryEquivalence;
import equivalence.TraceEquivalence;
import mealy.MealyHandler;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.Pair;
import net.automatalib.commons.util.Triple;
import net.automatalib.commons.util.mappings.Mapping;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.words.Alphabet;
import relation.DisablingRelation;

import java.util.*;

/**
 * {@code Minimiser} is the main entity implementing the algorithm for minimising Mealy Machines w.r.t. a given disabling relation. It makes use of different methods and provides
 * a sound, complete and potentially optimal solution. The optimised Mealy Machine computes the same output function as the initial one as checked in {@link equivalence.ComputedFunctionEquivalence#sameOutputFunctionComputed}.
 *
 * @author Mirela Mileva
 */
public class Minimiser {

    /**
     * @param m The Mealy Machine to optimise.
     * @return An optimal minimisation of the mealy machine w.r.t. {@code disablingRelation}.
     */
    public static FastMealy optimalMinimisation(FastMealy m, DisablingRelation disablingRelation) {

        List<FastMealy> checkedAutomata = new ArrayList<>();
        FastMealy currentMealy = m;
        currentMealy = MealyHandler.deleteIsolated(currentMealy);
        checkedAutomata.add(currentMealy);

        //while no more optimisation is possible, the procedure is executed
        do {
            //the initial state is not considered because trace equivalence is checked before the algorithm is called
            final Queue<Pair<FastMealyState<String>, FastMealyState<String>>> travOrder = calculatePairs(currentMealy.getStates(), currentMealy);

            FastMealy minimisedMealy = minimise(currentMealy, disablingRelation, travOrder);

            minimisedMealy = MealyHandler.eliminateDisabled(disablingRelation, minimisedMealy);
            minimisedMealy = TraceEquivalence.traceMinimise(minimisedMealy);
            MealyHandler.mergeFail(minimisedMealy, disablingRelation);

            //if the mealy has already been optimised once, do not add it to the set (corresponds to recursion base case)
            if (!isContained(minimisedMealy, checkedAutomata)) {
                checkedAutomata.add(minimisedMealy);
                currentMealy = minimisedMealy;

            } else {
                currentMealy = null;
            }

        } while (currentMealy != null);

        return chooseMinimalMealy(checkedAutomata);
    }

    /** Checks if the visual representation of a Mealy Machine is contained in a list, verifying three conditions:
     * <ol>
     *     <li>Trace equivalence with the comparing machine</li>
     *     <li>Equal number of transitions</li>
     *     <li>Equal number of states.</li>
     * </ol>
     * @return True, if a machine "looks the same" to other machine in the {@code checkedAutomata}, otherwise false.
     */
    private static boolean isContained(FastMealy currentMealy, List<FastMealy> checkedAutomata) {

        for (FastMealy mealy : checkedAutomata) {

            if (TraceEquivalence.checkEquivalence(mealy, currentMealy, mealy.getInputAlphabet()) &&
                    mealy.getStates().size() == currentMealy.getStates().size() &&
                    MealyHandler.numberOfTransitions(mealy) == MealyHandler.numberOfTransitions(currentMealy)) {
                    return true;
            }
        }

        return false;
    }


    /** {@code minimise} takes the calculated traversing order and handles each pair in it, to avoid possible unexpected behaviour mappings are used.
     */
    private static FastMealy minimise(FastMealy m, DisablingRelation disablingRelation, Queue<Pair<FastMealyState<String>, FastMealyState<String>>> traversingOrder) {

        final Alphabet<String> inputs = m.getInputAlphabet();

        FastMealy<String, String> tempM = new FastMealy<>(inputs);
        final Mapping<FastMealyState<String>, FastMealyState<String>> mapping =
                AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, m, inputs, tempM);

        Map<FastMealyState<String>, FastMealyState<String>> mToTempMMap = new HashMap<>();

        // start with the identity mapping
        for (FastMealyState<String> s : (Collection<FastMealyState>) m.getStates()) {
            mToTempMMap.put(s, mapping.get(s));
        }

        // actual "algorithm"
        while (!traversingOrder.isEmpty()) {
            final Pair<FastMealyState<String>, FastMealyState<String>> pair = traversingOrder.remove();
            final FastMealyState<String> tempQ1 = mToTempMMap.get(pair.getFirst());
            final FastMealyState<String> tempQ2 = mToTempMMap.get(pair.getSecond());

            // skip null elements resulted from splitting or identical state comparison
            if (tempQ1 == null || tempQ2 == null || tempQ1.equals(tempQ2)) {
                continue;
            }

            Pair resultingOrderAndMealy = equivalenceHandler(disablingRelation, inputs, tempM, mToTempMMap, tempQ1, tempQ2);
            tempM = (FastMealy<String, String>) resultingOrderAndMealy.getFirst();
            mToTempMMap = (Map<FastMealyState<String>, FastMealyState<String>>) resultingOrderAndMealy.getSecond();

        }

        return tempM;
    }

    /** For each pair of states checks if both are history equivalent
     *  {@link equivalence.HistoryEquivalence#checkHistoryEquivalence} and handles them accordingly.
     * @param inputs The considered alphabet.
     * @param tempM Current Mealy in the optimisation procedure.
     * @param mToTempMMap A mapping from the initial machine to {@code tempM}.
     * @param tempQ1 The first state in {@code tempM}.
     * @param tempQ2 The second state in {@code tempM}.
     */
    private static Pair equivalenceHandler(DisablingRelation disablingRelation, Alphabet<String> inputs, FastMealy<String, String> tempM,
                                           Map<FastMealyState<String>, FastMealyState<String>> mToTempMMap, FastMealyState<String> tempQ1, FastMealyState<String> tempQ2) {

        // either merge states if they are equivalent
        if (HistoryEquivalence.checkHistoryEquivalence(tempM, tempQ1, tempQ2, new ArrayList<>(), disablingRelation)) {

            Triple returnedMealyTriple = handleByEquivalence(disablingRelation, inputs, tempM, mToTempMMap, tempQ1, tempQ2);
            tempM = (FastMealy<String, String>) returnedMealyTriple.getFirst();
            mToTempMMap = (Map<FastMealyState<String>, FastMealyState<String>>) returnedMealyTriple.getThird();

        }
        //or split state
        else {
            Pair returnedMealyPair = handleByNoEquivalence(disablingRelation, inputs, tempM, mToTempMMap, tempQ1, tempQ2);
            tempM = (FastMealy<String, String>) returnedMealyPair.getFirst();
            mToTempMMap = (Map<FastMealyState<String>, FastMealyState<String>>) returnedMealyPair.getSecond();
        }

        return Pair.of(tempM, mToTempMMap);
    }

    /** Split both states and checks if some of the resulting states are equivalent. If so they are merged. Otherwise the split is reverted.
     */
    private static Pair handleByNoEquivalence(DisablingRelation disablingRelation, Alphabet<String> inputs, FastMealy<String, String> tempM, Map<FastMealyState<String>, FastMealyState<String>> mToTempMMap, FastMealyState<String> tempQ1, FastMealyState<String> tempQ2) {


        FastMealy<String, String> splitted = new FastMealy<>(inputs);

        Map<FastMealyState<String>, FastMealyState<String>> splitMapping = MealyHandler.copy(tempM, inputs, splitted);

        // splitState() gives us a new automaton
        Pair splitFirst = MealyHandler.splitState(splitMapping.get(tempQ1), splitted);
        ArrayList<FastMealyState> resultingStatesTempQ1 = (ArrayList<FastMealyState>) splitFirst.getFirst();

        if ((boolean) splitFirst.getSecond()) {
            splitted.removeState(splitMapping.get(tempQ1));
        }

        Pair splitSecond = MealyHandler.splitState(splitMapping.get(tempQ2), splitted);
        ArrayList<FastMealyState> resultingStatesTempQ2 = (ArrayList<FastMealyState>) splitSecond.getFirst();

        if ((boolean) splitSecond.getSecond()) {
            splitted.removeState(splitMapping.get(tempQ2));
        }

        //calculate all combinations between sets of resulting states from splitting
        List<Pair> crossPr = crossProduct(resultingStatesTempQ1, resultingStatesTempQ2);
        boolean splitSuccessful = false;

        for (Pair pairToCheck : crossPr) {

            if (HistoryEquivalence.checkHistoryEquivalence(splitted, (FastMealyState) pairToCheck.getFirst(), (FastMealyState) pairToCheck.getSecond(), new ArrayList<>(), disablingRelation)) {

                Triple equivalenceCheckResult = handleByEquivalence(disablingRelation, inputs, splitted, composeMap(mToTempMMap, splitMapping),
                        (FastMealyState) pairToCheck.getFirst(), (FastMealyState) pairToCheck.getSecond());

                splitMapping = (Map<FastMealyState<String>, FastMealyState<String>>) equivalenceCheckResult.getThird();
                boolean mergePossible = (boolean) equivalenceCheckResult.getSecond();
                splitted = (FastMealy<String, String>) equivalenceCheckResult.getFirst();

                if (mergePossible) {
                    splitSuccessful = true;
                    break;
                }

            }
        }
        //update the mapping in splitSuccess only if something has been merged, i.e. the splitting was successful
        return splitSuccess(tempM, mToTempMMap, splitted, splitMapping, splitSuccessful);

    }

    private static <K, V1, V2> Map<K, V2> composeMap(Map<K, V1> src, Map<V1, V2> map) {
        final Map<K, V2> result = Maps.newHashMapWithExpectedSize(src.size());

        for (Map.Entry<K, V1> e : src.entrySet()) {
            result.put(e.getKey(), map.get(e.getValue()));
        }

        return result;
    }

    private static Pair splitSuccess(FastMealy<String, String> tempM, Map<FastMealyState<String>, FastMealyState<String>> mToTempMMap, FastMealy<String, String> splitted, Map<FastMealyState<String>, FastMealyState<String>> splitMapping, boolean splitSuccessful) {
        if (splitSuccessful) {
            // update tempM
            tempM = splitted;
            for (FastMealyState<String> q : mToTempMMap.keySet()) {
                mToTempMMap.put(q, splitMapping.get(q));
            }
            return Pair.of(tempM, mToTempMMap);

        } else {
            return Pair.of(tempM, mToTempMMap);
        }
    }

    /** Merge both states and checks if the merge was successful. If not, the merge is reverted, i.e. the mapping is not updated.
     */
    private static Triple handleByEquivalence(DisablingRelation disablingRelation, Alphabet<String> inputs, FastMealy<String, String> tempM, Map<FastMealyState<String>, FastMealyState<String>> mToTempMMap, FastMealyState<String> tempQ1, FastMealyState<String> tempQ2) {

        final FastMealy<String, String> merged = new FastMealy(inputs);

        final Map<FastMealyState<String>, FastMealyState<String>> mergeMapping =
                MealyHandler.copy(tempM, inputs, merged);

        // where the equivalent state tempQ1 is removed and transitions of tempQ2 are updated
        MealyHandler.mergeStates(merged, mergeMapping.get(tempQ1), mergeMapping.get(tempQ2), disablingRelation);
        merged.removeState(mergeMapping.get(tempQ1));

        if (mergePossible(merged, tempM, disablingRelation)) {

            // update mapping, to map the just-removed copy of tempQ1 to the copy of the (now) equivalent tempQ2
            mergeMapping.put(tempQ1, mergeMapping.get(tempQ2));

            // update (global) mapping
            for (FastMealyState<String> q : mToTempMMap.keySet()) {
                mToTempMMap.put(q, mergeMapping.get(mToTempMMap.get(q)));
            }

            // update tempM
            tempM = merged;

            return Triple.of(tempM, true, mToTempMMap);
        }

        return Triple.of(tempM, false, mToTempMMap);
    }

    // initial state excluded
    private static Queue<Pair<FastMealyState<String>, FastMealyState<String>>> calculatePairs(Collection states, FastMealy currMealy) {
        Queue<Pair<FastMealyState<String>, FastMealyState<String>>> pairs = new ArrayDeque<>();
        for (FastMealyState firstState : (Collection<FastMealyState>) states) {
            for (FastMealyState secondState : (Collection<FastMealyState>) states) {
                if (!firstState.equals(secondState) && !(pairs.contains(Pair.of(secondState, firstState))) &&
                        !firstState.equals(currMealy.getInitialState()) && !secondState.equals(currMealy.getInitialState()))
                    pairs.add(Pair.of(firstState, secondState));
            }
        }
        return pairs;
    }

    /**
     * @return True, if merging does not violate the equivalence, otherwise false.
     */
    private static boolean mergePossible(FastMealy mealy, FastMealy tempM, DisablingRelation disablingRelation) {

        return ComputedFunctionEquivalence.sameOutputFunctionComputed(mealy, tempM, disablingRelation);
    }

    /**
     * @param obtainedAutomata All Mealy Machines resulted from minimisation procedure.
     * @return The most optimal solution, i.e. Mealy Machine with least states.
     */
    private static FastMealy chooseMinimalMealy(List<FastMealy> obtainedAutomata) {

        int minStates = Integer.MAX_VALUE;
        FastMealy finalMealy = null;
        for (FastMealy currMealy : obtainedAutomata) {
            if (currMealy.size() <= minStates) {
                minStates = currMealy.size();
                finalMealy = currMealy;
            }
        }

        return finalMealy;
    }

    private static ArrayList<Pair> crossProduct(ArrayList<FastMealyState> resultingStatesTempQ1, ArrayList<FastMealyState> resultingStatesTempQ2) {
        ArrayList<Pair> crossResult = new ArrayList<>();
        for (FastMealyState mealyState : resultingStatesTempQ1)
            for (FastMealyState fastMealyState : resultingStatesTempQ2)
                crossResult.add(Pair.of(mealyState, fastMealyState));
        return crossResult;
    }

}

