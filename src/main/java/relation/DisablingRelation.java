package relation;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.Pair;
import states.StateHandler;

import java.util.*;

/**
 * @author Mirela Mileva
 */
@SuppressWarnings("SuspiciousMethodCalls")
public class DisablingRelation extends Relation {

    public DisablingRelation(List<Pair> relationEntries) {
        super(relationEntries);
    }

    public Set<String> disables(String val) {
        return getSecondArgs(val);
    }

    /**
     * @return All symbols that are disabled for a specific state.
     */
    public Set<String> disabledInputs(FastMealyState state, FastMealy mealy) {
        Set<String> disabledFinal = new HashSet<>();
        Set<String> inputs = symbolsOnEachPath(state, mealy);
        for (String input : inputs) {
            Set<String> disabledInputs = disables(input);
            disabledFinal.addAll(disabledInputs);
        }
        return disabledFinal;
    }

    /**
     * @return A set of all symbols that can be read on each path leading to a specific state from the initial one.
     */
    public Set<String> symbolsOnEachPath(FastMealyState state, FastMealy mealy) {
        List<List<String>> allInputs = StateHandler.findAllPaths((FastMealyState) mealy.getInitialState(), state, mealy);
        Set<String> orthStates = new HashSet<>();

        for (Object alph : mealy.getInputAlphabet()) {
            boolean found = true;
            boolean flag = false;
            for (List<String> path : allInputs) {
                flag = true;
                if (!path.contains(alph)) {
                    found = false;
                    break;
                }
            }

            if (found && flag)
                orthStates.add((String) alph);
        }
        return orthStates;
    }

    public void setReflexive(FastMealy mealy) {

        for (Object symbol : mealy.getInputAlphabet()) {
            this.addToRel(Pair.of(symbol, symbol));
        }
    }
}
