package relation;

import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;

import java.util.*;

/**
 * Binary relation with custom properties.
 *
 * @author Mirela Mileva
 */
public class Relation {

    private final List<Pair> relationEntries;

    public Relation(List<Pair> relationEntries) {
        this.relationEntries = relationEntries;
    }

    public List<Pair> getRelationEntries() {
        return relationEntries;
    }

    public void addToRel(Pair pair) {
        relationEntries.add(pair);
    }

    public <I> Set getSecondArgs(I val) {
        Set<I> relEntries = new HashSet<>();
        Pair<I, I> curr;
        Iterator<Pair> itr = relationEntries.iterator();
        if (relationEntries.isEmpty()) {
            return new HashSet();
        }
        do {
            curr = itr.next();
            if (val.equals(curr.getFirst())) {
                relEntries.add(curr.getSecond());
            }
        } while (itr.hasNext());
        return relEntries;
    }

    public <I> boolean isReflexive(Alphabet<I> alphabet) {
        Iterator<I> itr = alphabet.iterator();
        I curr;
        while (itr.hasNext()) {
            curr = itr.next();
            Pair<I, I> alphPair = Pair.of(curr, curr);
            if (!relationEntries.contains(alphPair)) {
                return false;
            }
        }
        return true;
    }
}
