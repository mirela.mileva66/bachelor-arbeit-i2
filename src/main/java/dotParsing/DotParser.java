package dotParsing;

import com.alexmerz.graphviz.ParseException;
import com.alexmerz.graphviz.Parser;
import com.alexmerz.graphviz.objects.Graph;
import net.automatalib.automata.transducers.impl.FastMealy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/** The following class parses a DOT representation of a mealy machine into a FastMealy object.
 *  @author Mirela Mileva
 */
public class DotParser {

    /**
     * @param filePath Path to the DOT representation of a mealy machine.
     * @return Parsed file into a Mealy object.
     */

    public static FastMealy parseDot(String filePath) {
        FileReader fileToRead;
        File testFile = new File(filePath);
        FastMealy<String, String> resultingMealy = null;

        try {
            fileToRead = new FileReader(testFile);
            Parser mealyParse = new Parser();
            mealyParse.parse(fileToRead);
            ArrayList<Graph> readGraphs = mealyParse.getGraphs();

            //for each graph in the dot-file initialise a new mealy object
            for (Graph graph : readGraphs) {
                MachineInitialiser startMealy = new MachineInitialiser(graph);
                resultingMealy = startMealy.initMachine();
            }

        } catch (FileNotFoundException | ParseException e) {
            System.out.println(e.getMessage());
        }
        return resultingMealy;
    }
}