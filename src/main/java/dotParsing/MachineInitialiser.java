package dotParsing;

import com.alexmerz.graphviz.objects.Edge;
import com.alexmerz.graphviz.objects.Graph;
import com.alexmerz.graphviz.objects.Node;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.automata.transducers.impl.MealyTransition;
import net.automatalib.words.impl.GrowingMapAlphabet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * {@code MachineInitialiser} is the main entity we will be using to create a Mealy Machine object and initialise its alphabet, states and transitions.
 *
 * @author Mirela Mileva
 */

class MachineInitialiser<I, O> {
    private static Graph graph = null;

    public MachineInitialiser(Graph graph) {
        MachineInitialiser.graph = graph;
    }

    /**
     * The method is used to create the Mealy Machine object.
     *
     * @return Mealy Machine object.
     */
    public FastMealy<I, O> initMachine() {
        //generation of an empty Mealy Machine
        FastMealy<I, O> mutable = new FastMealy<>(new GrowingMapAlphabet<>());

        List<Node> nodes = graph.getNodes(false);
        List<Edge> edges = graph.getEdges();

        //initializing nodes
        Map<Node, FastMealyState<O>> nodeToState = new HashMap<>();
        boolean initialState = true;

        for (Node node : nodes) {

            FastMealyState<O> state = mutable.addState();
            nodeToState.put(node, state);
            if (initialState) {
                mutable.setInitialState(state);
                initialState = false;
            }
        }

        //initializing transitions
        for (Edge edge : edges) {

            //input : output notion is used in the dot-file
            String[] inOutSyms = edge.getAttribute("label").split(" : ");
            I inputSym = (I) inOutSyms[0];
            O outputSym = (O) inOutSyms[1];

            if (!mutable.getInputAlphabet().contains(inputSym)) {
                mutable.addAlphabetSymbol(inputSym);
            }

            FastMealyState<O> sourceNode = nodeToState.get(edge.getSource().getNode());
            FastMealyState<O> targetNode = nodeToState.get(edge.getTarget().getNode());
            MealyTransition<FastMealyState<O>, O> transition = new MealyTransition<>(targetNode, outputSym);
            mutable.setTransition(sourceNode, inputSym, transition);

        }
        return mutable;
    }
}