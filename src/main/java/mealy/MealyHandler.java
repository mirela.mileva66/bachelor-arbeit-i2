package mealy;

import com.google.common.collect.Maps;
import net.automatalib.automata.MutableAutomaton;
import net.automatalib.automata.UniversalAutomaton;
import net.automatalib.automata.base.fast.AbstractFastState;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.automata.transducers.impl.MealyTransition;
import net.automatalib.commons.util.Pair;
import net.automatalib.commons.util.mappings.Mapping;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.words.Alphabet;
import relation.DisablingRelation;
import states.StateHandler;
import states.TransDescr;

import java.util.*;

/**
 * {@code MealyHandler} provides different operations to handle Mealy Machines like merging states, splitting, elimination of transitions etc.
 *
 * @author Mirela Mileva
 */
@SuppressWarnings("SuspiciousMethodCalls")
public class MealyHandler {

    public static <S1, S2, I, T1, T2, SP, TP> Map<S1, S2> copy(UniversalAutomaton<S1, ? super I, T1, ? extends SP, ? extends TP> in,
                                                               Collection<? extends I> inputs,
                                                               MutableAutomaton<S2, I, T2, ? super SP, ? super TP> out) {

        //creates a low-level copy of the automaton
        final Mapping<S1, S2> orig = AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, in, inputs, out);
        final Map<S1, S2> copy = Maps.newHashMapWithExpectedSize(in.size());

        for (final S1 s : in) {
            copy.put(s, orig.get(s));
        }

        return copy;
    }


    /**
     * @return {@code currentAutomaton} without "disabled" transitions pursuant to the given disabling relation.
     */
    public static FastMealy eliminateDisabled(DisablingRelation disablingRelation, FastMealy currentAutomaton) {

        FastMealy mealy = new FastMealy(currentAutomaton.getInputAlphabet());
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, currentAutomaton, currentAutomaton.getInputAlphabet(), mealy);
        Collection<FastMealyState> states = mealy.getStates();
        int numTrans;

        do{
            numTrans = numberOfTransitions(mealy);
            //for each state delete all transitions that could not be ran because the input is disabled
            for (FastMealyState state : states) {
                Set<String> disabledForState = disablingRelation.disabledInputs(state, mealy);
                ArrayList<TransDescr> succTransitions = StateHandler.getSuccTransitions(state, mealy);

                for (TransDescr succTrans : succTransitions) {
                    if (disabledForState.contains(succTrans.getInput())) {
                        mealy.removeTransition(state, succTrans.getInput(), succTrans.getTransition());
                    }
                }
            }

        }while(numberOfTransitions(mealy) != numTrans);

        return mealy;
    }

    /**
     * Removal of states that can not be reached from the initial state.
     *
     * @return {@code currentAutomaton} without isolated states and their subsequent transitions.
     */
    public static FastMealy deleteIsolated(FastMealy currentAutomaton) {

        FastMealy mealy = new FastMealy(currentAutomaton.getInputAlphabet());
        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, currentAutomaton, currentAutomaton.getInputAlphabet(), mealy);

        int size = 1;
        Set<FastMealyState> statesToRemove = new HashSet<>();
        Map<FastMealyState, Integer> stateIDs = new HashMap<>();

        for (FastMealyState state : (Collection<FastMealyState>) mealy.getStates()) {
            stateIDs.put(state, state.getId());
        }

        //do the deletion in a loop while no more changes
        while (size != statesToRemove.size()) {
            size = statesToRemove.size();
            Collection<FastMealyState> states = mealy.getStates();

            for (FastMealyState state : states) {
                if (!state.equals(mealy.getInitialState())) {
                    ArrayList<TransDescr> transDescrs = StateHandler.getPredTransitions(state, mealy);

                    //delete the state if it has no predecessors, the only predecessor is the state itself or there is no path from the initial state to this state
                    if (transDescrs.size() == 0 || (transDescrs.size() == 1 && transDescrs.get(0).getState().equals(state))
                            || (StateHandler.findAllPaths((FastMealyState) mealy.getInitialState(), state, mealy).size() == 0)) {

                        for (TransDescr SucTransDescr : StateHandler.getSuccTransitions(state, mealy)) {
                            mealy.removeTransition(state, SucTransDescr.getInput(), SucTransDescr.getTransition());
                        }

                        statesToRemove.add(state);
                    }
                }
            }
        }

        for (FastMealyState state : statesToRemove) {
            mealy.removeState(state);
        }
        for (FastMealyState state : (Collection<FastMealyState>) mealy.getStates()) {
            state.setId(stateIDs.get(state));
        }

        return mealy;
    }

    /** If the given disabling relation is reflexive, the method merges all sink states to their predecessors.
     */
    public static void mergeFail(FastMealy mealy, DisablingRelation disablingRelation) {

        Map<FastMealyState, Boolean> handledPredecessors = new HashMap<>();

        for (FastMealyState state : (Collection<FastMealyState>) mealy.getStates()) {
            handledPredecessors.put(state, false);
        }

        boolean mergingNotPossible = false;

        if (disablingRelation.isReflexive(mealy.getInputAlphabet())) {

            Collection<FastMealyState<String>> states = mealy.getStates();

            for (FastMealyState state : states) {

                if (StateHandler.isFail(state, mealy) && !state.equals(mealy.getInitialState())) {
                    ArrayList<FastMealyState> predecessors = StateHandler.getPredStates(state, mealy);

                    for (FastMealyState pred : predecessors) {
                        if (handledPredecessors.get(pred) ||
                                StateHandler.getSuccTransitions(pred, mealy).size() > 1)
                            mergingNotPossible = true;
                    }
                    if (!mergingNotPossible) {

                        for (TransDescr transDescr : StateHandler.getPredTransitions(state, mealy)) {
                            handledPredecessors.put((FastMealyState) transDescr.getState(), true);
                            MealyTransition newTrans = new MealyTransition(transDescr.getState(), transDescr.getTransition().getOutput());
                            mealy.setTransition((AbstractFastState) transDescr.getState(), transDescr.getInput(), newTrans);
                            mealy.removeTransition(transDescr.getState(), transDescr.getInput(), transDescr.getTransition());
                        }

                        mealy.removeState(state);
                    }
                }
            }
        }

    }

    /** {@code mergeStates} takes two states in a mealy machine and merges them by deterministically copying all transitions from the first state
     * to the second one w.r.t. to a disabling relation.
     */
    public static void mergeStates(FastMealy mealy, FastMealyState firstState, FastMealyState secondState, DisablingRelation disablingRelation) {

        //remove all disabled transitions for the second state
        for (TransDescr transDescr: StateHandler.getSuccTransitions(secondState, mealy)) {
            if(disablingRelation.disabledInputs(secondState, mealy).contains(transDescr.getInput())) {
                mealy.removeTransition(secondState, transDescr.getInput(), mealy.getTransition(secondState, transDescr.getInput()));
            }
        }
        //copy all transitions to successors
        for (TransDescr transDescr : StateHandler.getSuccTransitions(firstState, mealy)) {
            MealyTransition firstSuccTrans = new MealyTransition(transDescr.getState(), transDescr.getTransition().getOutput());
            //copy only "enabled" transitions
            if (disablingRelation.disabledInputs(secondState, mealy).contains(transDescr.getInput()) &&
            !disablingRelation.disabledInputs(firstState, mealy).contains(transDescr.getInput())) {
//                mealy.removeTransition(secondState, transDescr.getInput(), mealy.getTransition(secondState, transDescr.getInput()));
                mealy.setTransition(secondState, transDescr.getInput(), firstSuccTrans);
                mealy.removeTransition(firstState, transDescr.getInput(), transDescr.getTransition());
            }
        }

        //copy all transitions to predecessors
        for (TransDescr transDescr : StateHandler.getPredTransitions(firstState, mealy)) {
            MealyTransition firstPredTrans = new MealyTransition(secondState, transDescr.getTransition().getOutput());
            mealy.setTransition((AbstractFastState) transDescr.getState(), transDescr.getInput(), firstPredTrans);
            mealy.removeTransition(transDescr.getState(), transDescr.getInput(), transDescr.getTransition());
        }

    }

    /** For each not selfloop unique preceding transition a new state is created and all subsequent transitions are copied or redirected, respectively.
     * @param state The state to be split.
     * @return A pair consisting of all states resulted from the splitting and a flag indicating if a split has been performed.
     */
    public static Pair splitState(FastMealyState state, FastMealy mealy) {
        int sameTransFound;

        ArrayList<TransDescr> predecessor = StateHandler.getPredTransitions(state, mealy);
        ArrayList<TransDescr> successors = StateHandler.getSuccTransitions(state, mealy);

        ArrayList<FastMealyState> resultingStates = new ArrayList<>();
        Map<Pair, MealyTransition> consideredTrans = new HashMap<>();

        ArrayList<Pair<String, String>> consideredInputOutput = new ArrayList<>();
        //check if the state can be split
        if (predecessor.size() > 1) {
            for (TransDescr predTransDescr : predecessor) {
                //ignore self-loops
                if (!predTransDescr.getState().equals(state)) {
                    sameTransFound = 0;

                    Pair toCheck = Pair.of(predTransDescr.getInput(), predTransDescr.getTransition().getOutput());
                    //if a state has already been created for the specific input, just redirect transitions and do not create a new state
                    if (consideredInputOutput.contains(toCheck)) {
                        MealyTransition transition = (MealyTransition) mealy.copyTransition(consideredTrans.get(toCheck), consideredTrans.get(toCheck).getSuccessor());
                        mealy.setTransition((AbstractFastState) predTransDescr.getState(), predTransDescr.getInput(), transition);
                        sameTransFound = 1;
                    }

                    if (sameTransFound == 0) {

                        FastMealyState newState = (FastMealyState) mealy.addState();
                        MealyTransition newPredecessorTrans = new MealyTransition(newState, predTransDescr.getTransition().getOutput());
                        mealy.setTransition((AbstractFastState) predTransDescr.getState(), predTransDescr.getInput(), newPredecessorTrans);

                        for (TransDescr succTransDescr : successors) {

                            if (succTransDescr.getState().equals(state)) {
                                MealyTransition newTrans = new MealyTransition(newState, succTransDescr.getTransition().getOutput());
                                mealy.setTransition(newState, succTransDescr.getInput(), newTrans);

                            } else {
                                MealyTransition newTrans = new MealyTransition(succTransDescr.getState(), succTransDescr.getTransition().getOutput());
                                mealy.setTransition(newState, succTransDescr.getInput(), newTrans);
                            }
                        }

                        resultingStates.add(newState);

                        Pair newConsiderationTuple = Pair.of(predTransDescr.getInput(), predTransDescr.getTransition().getOutput());

                        consideredInputOutput.add(newConsiderationTuple);
                        consideredTrans.put(newConsiderationTuple, newPredecessorTrans);
                    }
                }
            }

            return Pair.of(resultingStates, true);

        } else {

            resultingStates.add(state);

            return Pair.of(resultingStates, false);
        }
    }

    /**
     * @return The number of all transitions in the Mealy Machine.
     */
    public static int numberOfTransitions(FastMealy mealy) {

        int numberOfTrans = 0;

        for (FastMealyState state : (Collection<FastMealyState>) mealy.getStates()) {
            for (String currentWord : (Alphabet<String>) mealy.getInputAlphabet()) {
                numberOfTrans += mealy.getTransitions(state, currentWord).size();
            }
        }

        return numberOfTrans;
    }

}
