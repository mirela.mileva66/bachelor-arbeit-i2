package equivalence;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.automata.transducers.impl.MealyTransition;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import relation.DisablingRelation;
import states.StateHandler;
import states.TransDescr;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Set;

/**
 * The {@code HistoryEquivalence} class is used to check if two states in a Mealy Machine are history equivalent w.r.t. a disabling relation,
 * only "enabled" transitions are considered.
 *
 * @author Mirela Mileva
 */
@SuppressWarnings("SuspiciousMethodCalls")
public abstract class HistoryEquivalence {

    /**
     * @param consideredPairs A set where all checked pairs are added.
     * @return True if both states are history equivalent, otherwise false.
     */
    public static boolean checkHistoryEquivalence(FastMealy mealy, FastMealyState firstState, FastMealyState secondState, ArrayList<Pair> consideredPairs, DisablingRelation disablingRelation) {
        boolean returnValue = true;

        ArrayList<TransDescr> successorsFirst = StateHandler.getSuccTransitions(firstState, mealy);
        ArrayList<TransDescr> successorsSecond = StateHandler.getSuccTransitions(secondState, mealy);

        Pair pairToConsider = Pair.of(firstState, secondState);

        Set<String> disabledFirst = disablingRelation.disabledInputs(firstState, mealy);
        Set<String> disabledSecond = disablingRelation.disabledInputs(secondState, mealy);

        if ((firstState == null && secondState != null) || (firstState != null && secondState == null)) {
            return false;
        }

        //if a pair have already been considered, a cycle is encountered
        if (!consideredPairs.contains(pairToConsider)) {
            consideredPairs.add(pairToConsider);

            //check in both directions if an enabled successor transition of the one state is disabled for the other one
            for (TransDescr transDescrFirst : successorsFirst) {
                if (!containsTrInput(mealy, (String) transDescrFirst.getInput(), secondState) && !disabledFirst.contains(transDescrFirst.getInput())) {
                    if (disabledSecond.contains(transDescrFirst.getInput())) {
                        successorsFirst.stream().skip(1);
                    } else return false;
                }
            }

            for (TransDescr transDescrSecond : successorsSecond) {
                if (!containsTrInput(mealy, (String) transDescrSecond.getInput(), firstState) && !disabledSecond.contains(transDescrSecond.getInput())) {
                    if (disabledFirst.contains(transDescrSecond.getInput())) {
                        successorsFirst.stream().skip(1);
                    } else return false;
                }
            }

            //if both states can read the same input, check if the output is equal and the successor states are equivalent as well
            for (String input : (Alphabet<String>) mealy.getInputAlphabet()) {
                if (containsTrInput(mealy, input, firstState) && containsTrInput(mealy, input, secondState) &&
                        !(disabledFirst.contains(input)) && !(disabledSecond.contains(input))) {
                    MealyTransition mealyTransitionFirst = (MealyTransition) mealy.getTransition(Objects.requireNonNull(firstState), input);
                    MealyTransition mealyTransitionSecond = (MealyTransition) mealy.getTransition(Objects.requireNonNull(secondState), input);
                    if (!Objects.requireNonNull(mealyTransitionFirst).getOutput().equals(Objects.requireNonNull(mealyTransitionSecond).getOutput())) {
                        return false;
                    }
                    returnValue = checkHistoryEquivalence(mealy, (FastMealyState) mealyTransitionFirst.getSuccessor(), (FastMealyState) mealyTransitionSecond.getSuccessor(), consideredPairs, disablingRelation);
                }
            }
        }
        return returnValue;
    }

    /**
     * @return True, if mealy has a transition from the given state with the given input, otherwise false.
     */
    private static boolean containsTrInput(FastMealy mealy, String input, FastMealyState state) {

        return !(mealy.getTransition(state, input) == null);

    }
}
