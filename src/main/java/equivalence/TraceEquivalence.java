package equivalence;

import net.automatalib.automata.AutomatonCreator;
import net.automatalib.automata.concepts.StateIDs;
import net.automatalib.automata.transducers.MutableMealyMachine;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.util.automata.equivalence.NearLinearEquivalenceTest;
import net.automatalib.util.partitionrefinement.PaigeTarjan;
import net.automatalib.util.partitionrefinement.PaigeTarjanExtractors;
import net.automatalib.util.partitionrefinement.PaigeTarjanInitializers;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import java.util.function.Function;

/**
 * {@code TraceEquivalence} program is used to minimise a Mealy Machine by merging trace equivalent states using partition refinement.
 */
//
@SuppressWarnings("ALL")
public class TraceEquivalence {

    /**
     * @return A maximal minimised Mealy Machine pursuant to trace equivalence.
     * @see <a href="http://learnlib.github.io/automatalib/maven-site/0.7.0/apidocs/net/automatalib/util/partitionrefinement/PaigeTarjan.html">
     * http://learnlib.github.io/automatalib/maven-site/0.7.0/apidocs/net/automatalib/util/partitionrefinement/PaigeTarjan.html</a>
     */
    public static FastMealy traceMinimise(FastMealy mealy) {
        FastMealy resMealy = (FastMealy) minimisePartMealy(FastMealy<String, String>::new, mealy);
        return resMealy;
    }

    private static <S, T> MutableMealyMachine<S, String, ?, String> minimisePartMealy(AutomatonCreator<? extends MutableMealyMachine<S, String, T, String>, String> creator, FastMealy mealy) {
        Alphabet alphabet = mealy.getInputAlphabet();
        final MutableMealyMachine<S, String, T, String> machine = mealy;
        final Function<? super S, Object> initialClassification =
                PaigeTarjanInitializers.AutomatonInitialPartitioning.BY_TRANSITION_PROPERTIES.initialClassifier(machine, alphabet);

        final PaigeTarjan pt = new PaigeTarjan();

        final StateIDs<S> stateIds =
                PaigeTarjanInitializers.initDeterministic(pt, machine, alphabet, initialClassification, 'z');

        pt.initWorklist(false);
        pt.computeCoarsestStablePartition();

        final MutableMealyMachine<S, String, ?, String> prunedResult = PaigeTarjanExtractors.toDeterministic(pt,
                creator,
                alphabet,
                machine,
                stateIds,
                null,
                machine::getTransitionProperty,
                false);

        return prunedResult;
    }


    /**
     * @return True if both Mealy Machines are trace equivalent, otherwise false.
     */
    public static boolean checkEquivalence(FastMealy firstMealy, FastMealy secondMealy, Alphabet<String> alphabet) {

        //is there a word which separates both mealies?
        Word<String> sepWord = NearLinearEquivalenceTest.findSeparatingWord(firstMealy, secondMealy, alphabet, false);

        return (sepWord == null);
    }
}
