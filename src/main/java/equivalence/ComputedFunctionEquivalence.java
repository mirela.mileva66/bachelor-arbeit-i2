package equivalence;


import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import relation.DisablingRelation;
import states.StateHandler;

import java.util.*;

/**
 * The program is used to check if two Mealy Machines compute the same output-function for each input sequence w.r.t. a disabling relation.
 *
 * @author Mirela Mileva
 */
public class ComputedFunctionEquivalence {

    /**
     * @param firstMealy        First Mealy Machine to consider.
     * @param secondMealy       Second Mealy Machine to consider.
     * @param disablingRelation Disabling relation for calculating all "enabled" input sequence.
     * @return True if both Mealy Machines compute the same output-function for each "enabled" input sequence, otherwise false.
     */
    public static boolean sameOutputFunctionComputed(FastMealy firstMealy, FastMealy secondMealy, DisablingRelation disablingRelation) {

        boolean equivalent = true;

        List<List<String>> allInputSequencesFirst = allWords((FastMealyState) firstMealy.getInitialState(), firstMealy);
        List<List<String>> allInputSequencesSecond = allWords((FastMealyState) secondMealy.getInitialState(), secondMealy);

        //all "enabled" words w.r.t. a disabling relation
        List<List<String>> prunedSequencesFirst = pruneDisabledPaths(allInputSequencesFirst, disablingRelation);
        List<List<String>> prunedSequencesSecond = pruneDisabledPaths(allInputSequencesSecond, disablingRelation);

        //check in both directions if the mealy machines compute the same output for each "enabled" input sequence
        for (List<String> sequence : prunedSequencesFirst) {
            if (!Objects.requireNonNull(firstMealy.computeOutput(sequence)).equals(secondMealy.computeOutput(sequence))) {
                equivalent = false;
            }
        }

        for (List<String> sequence : prunedSequencesSecond) {
            if (!Objects.requireNonNull(firstMealy.computeOutput(sequence)).equals(secondMealy.computeOutput(sequence))) {
                equivalent = false;
            }
        }

        return equivalent;
    }

    /**
     * @return All input sequences.
     */
    private static List<List<String>> allWords(FastMealyState initialState, FastMealy mealy) {

        List<List<String>> finalResult = new ArrayList<>();

        for (FastMealyState state : (Collection<FastMealyState>) mealy.getStates()) {
            List<List<String>> allPathsBetweenStates = StateHandler.findAllPaths(initialState, state, mealy);

            allPathsBetweenStates.removeIf(List::isEmpty);

            finalResult.addAll(allPathsBetweenStates);
        }

        return finalResult;
    }

    /**
     * @return All "enabled" input sequences.
     */
    private static List<List<String>> pruneDisabledPaths(List<List<String>> allPaths, DisablingRelation disablingRelation) {

        Iterator pathIterator = allPaths.iterator();

        while (pathIterator.hasNext()) {
            boolean deleteFlag = false;
            List<String> currentPath = (List<String>) pathIterator.next();

            for (int i = 0; i < currentPath.size() - 1; i++) {
                for (int j = i + 1; j < currentPath.size(); j++) {
                    if (disablingRelation.disables(currentPath.get(i)).contains(currentPath.get(j))) {
                        deleteFlag = true;
                    }
                }
            }

            if (deleteFlag) {
                pathIterator.remove();
            }
        }

        return allPaths;
    }
}


