import dotParsing.DotParser;
import equivalence.TraceEquivalence;
import minimisation.Minimiser;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.commons.util.Pair;
import net.automatalib.serialization.dot.DefaultDOTVisualizationHelper;
import net.automatalib.serialization.dot.GraphDOT;
import net.automatalib.visualization.dot.DOT;
import relation.DisablingRelation;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;


class Main {
    /**
     * The main method reads a file path from the console, as well as a disabling relation. It visualises the Mealy Machine from the DOT file initially,
     * then it minimises the Mealy Machines and visualises the computed output.
     */
    public static void main(String[] args) {

        FastMealy parsedMealy = null;
        DisablingRelation disablingRelation = null;

        try {
            System.out.println("Enter the path name of a dot-specified mealy you want to optimise:");
            Scanner scanner = new Scanner(System.in);
            parsedMealy = DotParser.parseDot(scanner.nextLine());

            System.out.println("Enter the disabling relation, 0 to exit");
            disablingRelation = new DisablingRelation(new ArrayList());
            boolean pairs = true;

            while (pairs){
                System.out.println("Enter pair in the following format \"input,input\" (0 to exit):");
                String enteredDisabling = scanner.nextLine();

                if(enteredDisabling.equals("0")) {
                    pairs = false;
                } else {
                    String[] pair = enteredDisabling.split(",");
                    disablingRelation.addToRel(Pair.of(pair[0], pair[1]));
                }
            }

            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Mealy before optimisation
        try (Writer w = DOT.createDotWriter(true)) {
            GraphDOT.write(parsedMealy, parsedMealy.getInputAlphabet(), w, new MyTitleHelper<>("Mealy Machine before optimisation:"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //optimise Mealy
        parsedMealy = TraceEquivalence.traceMinimise(parsedMealy);
        parsedMealy = Minimiser.optimalMinimisation(parsedMealy, disablingRelation);

        //Mealy after optimisation
        try (Writer w = DOT.createDotWriter(true)) {
            GraphDOT.write(parsedMealy, parsedMealy.getInputAlphabet(), w, new MyTitleHelper<>("Mealy Machine after optimisation:"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    private static class MyTitleHelper<N, E> extends DefaultDOTVisualizationHelper<N, E> {

        private final String title;

        MyTitleHelper(String title) {
            this.title = title;
        }

        @Override
        public void writePreamble(Appendable a) {
            try {
                a.append("graph [label=\"").append(title).append("\", labelloc=t, fontsize=20];\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}



