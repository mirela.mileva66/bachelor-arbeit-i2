package states;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.nid.AbstractMutableNumericID;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("ALL")
public class StateHandlerTest {

    @Test
    public void findAllPathWords() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");
        testInputs.add("ee");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();
        final FastMealyState q5 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q3, "yy");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q3, "dd", q3, "yy");
        givenMealy.setTransition(q3, "cc", q4, "yy");
        givenMealy.setTransition(q4, "ee", q5, "yy");
        givenMealy.setTransition(q2, "dd", q5, "yy");

        List<String> firstPath = Arrays.asList("aa", "cc", "dd", "cc", "ee");
        List<String> secondPath = Arrays.asList("aa", "cc", "cc", "ee");
        List<String> thirdPath = Arrays.asList("aa", "dd");
        List<String> fourthPath = Arrays.asList("bb", "dd", "cc", "ee");
        List<String> fifthPath = Arrays.asList("bb", "cc", "ee");

        List<List<String>> expected = new ArrayList<>();
        expected.add(firstPath);
        expected.add(secondPath);
        expected.add(thirdPath);
        expected.add(fourthPath);
        expected.add(fifthPath);

        List<List<String>> actual = StateHandler.findAllPaths(q1, q5, givenMealy);

        assertEquals(actual, expected);

    }

    @Test
    public void getSuccTransitions() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q3, "yy");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q3, "dd", q3, "yy");

        ArrayList<TransDescr> expected = new ArrayList<>();
        expected.add(new TransDescr(givenMealy.getTransition(q1, "aa"), "aa", q2));
        expected.add(new TransDescr(givenMealy.getTransition(q1, "bb"), "bb", q3));


        ArrayList<TransDescr> actual = StateHandler.getSuccTransitions(q1, givenMealy);

        assertEquals(actual, expected);

    }

    @Test
    public void getPredTransitions() {
        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q3, "yy");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q3, "dd", q3, "yy");

        ArrayList<TransDescr> expected = new ArrayList<>();
        expected.add(new TransDescr(givenMealy.getTransition(q1, "bb"), "bb", q1));
        expected.add(new TransDescr(givenMealy.getTransition(q2, "cc"), "cc", q2));
        expected.add(new TransDescr(givenMealy.getTransition(q3, "dd"), "dd", q3));

        ArrayList<TransDescr> actual = StateHandler.getPredTransitions(q3, givenMealy);

        assertEquals(actual, expected);
    }

    @Test
    public void getSuccStates() {
        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q3, "yy");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q3, "dd", q3, "yy");

        ArrayList<FastMealyState> expected = new ArrayList<>();
        expected.add(q2);
        expected.add(q3);


        ArrayList<FastMealyState> actual = StateHandler.getSuccStates(q1, givenMealy);

        assertEquals(actual, expected);
    }

    @Test
    public void getPredStates() {
        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q3, "yy");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q3, "dd", q3, "yy");

        ArrayList<Integer> expectedIds = new ArrayList<>();
        expectedIds.add(q2.getId());
        expectedIds.add(q1.getId());
        expectedIds.add(q3.getId());

        ArrayList<FastMealyState> actual = StateHandler.getPredStates(q3, givenMealy);
        List<Integer> actualIds = actual.stream().map(AbstractMutableNumericID::getId).collect(Collectors.toList());

        assertEquals(new HashSet<>(expectedIds), new HashSet<>(actualIds));
    }

    @Test
    public void isFail() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");


        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();
        final FastMealyState q6 = givenMealy.addState();

        //q4 and q6 are sink states
        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "cc", q3, "yy");
        givenMealy.setTransition(q2, "cc", q4, "rr");
        givenMealy.setTransition(q3, "cc", q4, "yy");
        givenMealy.setTransition(q3, "aa", q6, "xx");

        assertTrue(StateHandler.isFail(q4, givenMealy) && !StateHandler.isFail(q3, givenMealy));
    }
}