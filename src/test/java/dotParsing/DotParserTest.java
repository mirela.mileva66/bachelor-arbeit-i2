package dotParsing;

import net.automatalib.automata.transducers.impl.FastMealy;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DotParserTest {

    @Test
    public void testMachineInitialisation() {

        FastMealy parsedMealy = DotParser.parseDot("src/main/resources/synthesized2.dot");

        assertNotNull(parsedMealy);
    }

    @Test
    public void testMachineInitialisationNull() {

        //empty path test
        FastMealy parsedMealy = DotParser.parseDot("");

        assertNull(parsedMealy);
    }

}