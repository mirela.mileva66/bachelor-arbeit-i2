package equivalence;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("ALL")
public class TraceEquivalenceTest {

    @Test
    public void traceMinimise() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        // states q1, q2 are equivalent
        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q1, "cc", q1, "xx");
        givenMealy.setTransition(q2, "aa", q1, "xx");
        givenMealy.setTransition(q2, "bb", q3, "yy");
        givenMealy.setTransition(q2, "cc", q2, "xx");
        givenMealy.setTransition(q3, "aa", q3, "xx");
        givenMealy.setTransition(q3, "bb", q3, "xx");
        givenMealy.setTransition(q3, "cc", q3, "xx");

        Alphabet testInputsExpected = new GrowingMapAlphabet<>();

        testInputsExpected.add("aa");
        testInputsExpected.add("bb");
        testInputsExpected.add("cc");


        final FastMealy<String, String> expectedMealy = new FastMealy<>(testInputsExpected);

        final FastMealyState p1 = expectedMealy.addInitialState();
        final FastMealyState p2 = expectedMealy.addState();

        expectedMealy.setTransition(p1, "aa", p1, "xx");
        expectedMealy.setTransition(p1, "cc", p1, "xx");
        expectedMealy.setTransition(p1, "bb", p2, "yy");
        expectedMealy.setTransition(p2, "aa", p2, "xx");
        expectedMealy.setTransition(p2, "bb", p2, "xx");
        expectedMealy.setTransition(p2, "cc", p2, "xx");

        givenMealy = TraceEquivalence.traceMinimise(givenMealy);

        assertTrue(TraceEquivalence.checkEquivalence(givenMealy, expectedMealy, testInputs)
                && givenMealy.getStates().size() == 2);

    }

    @Test
    public void checkEquivalence() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");

        final FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        // states q1, q2 are equivalent
        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();

        givenMealy.setTransition(q1, "aa", q1, "xx");
        givenMealy.setTransition(q1, "cc", q2, "xx");
        givenMealy.setTransition(q2, "aa", q2, "xx");
        givenMealy.setTransition(q2, "cc", q1, "xx");
        givenMealy.setTransition(q3, "bb", q3, "xx");

        Alphabet testInputsExpected = new GrowingMapAlphabet<>();

        testInputsExpected.add("aa");
        testInputsExpected.add("bb");
        testInputsExpected.add("cc");


        final FastMealy<String, String> expectedMealy = new FastMealy<>(testInputsExpected);

        final FastMealyState p1 = expectedMealy.addInitialState();


        expectedMealy.setTransition(p1, "aa", p1, "xx");
        expectedMealy.setTransition(p1, "cc", p1, "xx");


        assertTrue(TraceEquivalence.checkEquivalence(givenMealy, expectedMealy, testInputs));
    }
}