package equivalence;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;
import relation.DisablingRelation;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HistoryEquivalenceTest {

    @Test
    public void checkHistoryEquivalence() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy testMealy = new FastMealy<>(testInputs);

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));
        disRelReflexive.addToRel(Pair.of("dd", "dd"));

        DisablingRelation disRelIrReflexive = new DisablingRelation(new ArrayList<>());

        disRelIrReflexive.addToRel(Pair.of("aa", "aa"));
        disRelIrReflexive.addToRel(Pair.of("cc", "cc"));
        disRelIrReflexive.addToRel(Pair.of("dd", "dd"));

        final FastMealyState<String> q1 = (FastMealyState<String>) testMealy.addState();
        testMealy.setInitialState(q1);
        final FastMealyState<String> q2 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q3 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q4 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q5 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q6 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q7 = (FastMealyState<String>) testMealy.addState();


        testMealy.setTransition(q1, "aa", q2, "dd");
        testMealy.setTransition(q1, "bb", q3, "dd");
        testMealy.setTransition(q2, "dd", q4, "zz");
        testMealy.setTransition(q3, "dd", q7, "zz");
        testMealy.setTransition(q2, "bb", q5, "zz");
        testMealy.setTransition(q3, "aa", q6, "zz");
        testMealy.setTransition(q4, "cc", q4, "zz");
        testMealy.setTransition(q7, "cc", q7, "zz");

        boolean equivalentStatesReflexive = HistoryEquivalence.checkHistoryEquivalence(testMealy, q2, q3, new ArrayList<>(), disRelReflexive);
        boolean equivalentStatesIrReflexive = HistoryEquivalence.checkHistoryEquivalence(testMealy, q2, q3, new ArrayList<>(), disRelIrReflexive);

        assertTrue(equivalentStatesReflexive);
        assertFalse(equivalentStatesIrReflexive);
    }

    @Test
    public void checkHistoryEquivalenceByNotRemovedDisabledTrans() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");


        FastMealy testMealy = new FastMealy<>(testInputs);

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));


        final FastMealyState<String> q1 = (FastMealyState<String>) testMealy.addState();
        testMealy.setInitialState(q1);
        final FastMealyState<String> q2 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q3 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q4 = (FastMealyState<String>) testMealy.addState();



        testMealy.setTransition(q1, "aa", q2, "dd");
        testMealy.setTransition(q1, "bb", q3, "dd");
        testMealy.setTransition(q2, "aa", q4, "zz");
        testMealy.setTransition(q3, "bb", q4, "zz");


        boolean equivalentStatesYes= HistoryEquivalence.checkHistoryEquivalence(testMealy, q2, q3, new ArrayList<>(), disRelReflexive);

        assertTrue(equivalentStatesYes);
    }

    @Test
    public void checkHistoryEquivalenceByRemovedDisabledTrans() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");


        FastMealy testMealy = new FastMealy<>(testInputs);

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));


        final FastMealyState<String> q1 = (FastMealyState<String>) testMealy.addState();
        testMealy.setInitialState(q1);
        final FastMealyState<String> q2 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q3 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q4 = (FastMealyState<String>) testMealy.addState();



        testMealy.setTransition(q1, "aa", q2, "dd");
        testMealy.setTransition(q1, "bb", q3, "dd");


        boolean equivalentStatesYes = HistoryEquivalence.checkHistoryEquivalence(testMealy, q2, q3, new ArrayList<>(), disRelReflexive);

        assertTrue(equivalentStatesYes);

    }
}