package minimisation;

import dotParsing.DotParser;
import equivalence.ComputedFunctionEquivalence;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.Pair;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;
import relation.DisablingRelation;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

@SuppressWarnings("ALL")
public class MinimiserTest {

    @Test
    public void optimalMinimisationWithOutSplitting() {

        DisablingRelation disablingRelation = new DisablingRelation(new ArrayList<>());

        disablingRelation.addToRel(Pair.of("aa", "ee"));
        disablingRelation.addToRel(Pair.of("bb", "dd"));


        final Alphabet alphabet = new GrowingMapAlphabet();
        alphabet.add("aa");
        alphabet.add("bb");
        alphabet.add("cc");
        alphabet.add("dd");
        alphabet.add("ee");

        FastMealy<String, String> dfa = new FastMealy(alphabet);

        final FastMealyState<String> q1 = dfa.addState();
        dfa.setInitialState(q1);
        final FastMealyState<String> q2 = dfa.addState();
        final FastMealyState<String> q3 = dfa.addState();
        final FastMealyState<String> q4 = dfa.addState();
        final FastMealyState<String> q5 = dfa.addState();
        final FastMealyState<String> q6 = dfa.addState();


        dfa.setTransition(q1, "aa", q2, "tt");
        dfa.setTransition(q1, "bb", q3, "tt");
        dfa.setTransition(q2, "cc", q2, "qq");
        dfa.setTransition(q3, "cc", q3, "qq");
        dfa.setTransition(q2, "dd", q4, "tt");
        dfa.setTransition(q3, "ee", q4, "tt");

        FastMealy<String, String> expected = new FastMealy<>(alphabet);

        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, dfa, alphabet, expected);

        expected = Minimiser.optimalMinimisation(dfa, disablingRelation);

        assertTrue(ComputedFunctionEquivalence.sameOutputFunctionComputed(expected, dfa, disablingRelation));
    }

    @Test
    public void optimalMinimisationWithSplitting() {

        DisablingRelation disRel = new DisablingRelation(new ArrayList());

        disRel.addToRel(Pair.of("a1","a1"));
        disRel.addToRel(Pair.of("a2","a2"));
        disRel.addToRel(Pair.of("a3","a3"));
        disRel.addToRel(Pair.of("a4","a4"));
        disRel.addToRel(Pair.of("b","b"));

        final Alphabet alphabet = new GrowingMapAlphabet();
        alphabet.add("a1");
        alphabet.add("a2");
        alphabet.add("a3");
        alphabet.add("a4");
        alphabet.add("b");

        FastMealy<String, String> mealyToTest = new FastMealy(alphabet);

        final FastMealyState<String> p1 = mealyToTest.addState();
        mealyToTest.setInitialState(p1);
        final FastMealyState<String> p2 = mealyToTest.addState();
        final FastMealyState<String> p3 = mealyToTest.addState();
        final FastMealyState<String> p4 = mealyToTest.addState();
        final FastMealyState<String> p5 = mealyToTest.addState();
        final FastMealyState<String> p6 = mealyToTest.addState();


        mealyToTest.setTransition(p1, "a1", p2, "r1");
        mealyToTest.setTransition(p1, "a3", p4, "r1");
        mealyToTest.setTransition(p1, "a2", p3, "r1");
        mealyToTest.setTransition(p1, "a4", p5, "r1");
        mealyToTest.setTransition(p2, "b", p4, "r2");
        mealyToTest.setTransition(p3, "b", p5, "r2");
        mealyToTest.setTransition(p4, "b", p6, "r3");
        mealyToTest.setTransition(p5, "b", p6, "r4");

        FastMealy<String, String> expectedSecond = new FastMealy<>(alphabet);

        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.DFS, mealyToTest, alphabet, expectedSecond);

        expectedSecond = Minimiser.optimalMinimisation(mealyToTest, disRel);

        assertTrue(ComputedFunctionEquivalence.sameOutputFunctionComputed(mealyToTest, expectedSecond, disRel));
    }

    @Test
    public void optimalMinimisationFromDot() {

        DisablingRelation disablingRelation = new DisablingRelation(new ArrayList());

        FastMealy parsedMealy = DotParser.parseDot("src/main/resources/synthesized.dot");

        disablingRelation.setReflexive(parsedMealy);

        FastMealy<String, String> expected = new FastMealy<>(parsedMealy.getInputAlphabet());

        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, parsedMealy, parsedMealy.getInputAlphabet(), expected);

        expected = Minimiser.optimalMinimisation(parsedMealy, disablingRelation);

        assertTrue(ComputedFunctionEquivalence.sameOutputFunctionComputed(expected, parsedMealy, disablingRelation));
    }

    @Test
    public void checkHistoryEquivalenceByNotRemovedDisabledTrans() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");

        FastMealy testMealy = new FastMealy<>(testInputs);

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));

        final FastMealyState<String> q1 = (FastMealyState<String>) testMealy.addState();
        testMealy.setInitialState(q1);
        final FastMealyState<String> q2 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q3 = (FastMealyState<String>) testMealy.addState();
        final FastMealyState<String> q4 = (FastMealyState<String>) testMealy.addState();

        testMealy.setTransition(q1, "aa", q2, "dd");
        testMealy.setTransition(q1, "bb", q3, "dd");
        testMealy.setTransition(q2, "aa", q4, "zz");
        testMealy.setTransition(q3, "bb", q4, "zz");
        testMealy.setTransition(q4, "cc", q4, "zz");

        testMealy = Minimiser.optimalMinimisation(testMealy, disRelReflexive);

    }

}
