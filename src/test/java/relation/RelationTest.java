package relation;

import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@SuppressWarnings("ALL")
public class RelationTest {

    @Test
    public void addToRel() {

        Relation actualRelation = new Relation(new ArrayList<>());
        actualRelation.addToRel(Pair.of("aa", "aa"));

        assertEquals(1, actualRelation.getRelationEntries().size());
    }

    @Test
    public void getSecondArgs() {

        Relation actualRelation = new Relation(new ArrayList<>());
        actualRelation.addToRel(Pair.of("aa", "aa"));
        actualRelation.addToRel(Pair.of("bb", "bb"));
        actualRelation.addToRel(Pair.of("aa", "cc"));
        actualRelation.addToRel(Pair.of("aa", "dd"));

        Set<String> expected = new HashSet<>();
        expected.add("aa");
        expected.add("cc");
        expected.add("dd");

        assertEquals(actualRelation.getSecondArgs("aa"), expected);
    }

    @Test
    public void isReflexive() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        Relation actualRelation = new Relation(new ArrayList<>());
        actualRelation.addToRel(Pair.of("aa", "aa"));
        actualRelation.addToRel(Pair.of("bb", "bb"));
        actualRelation.addToRel(Pair.of("aa", "cc"));

        assertFalse(actualRelation.isReflexive(testInputs));
    }
}