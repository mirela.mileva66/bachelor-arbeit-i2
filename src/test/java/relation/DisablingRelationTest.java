package relation;

import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("ALL")
public class DisablingRelationTest {

    @Test
    public void disables() {

        DisablingRelation disRel = new DisablingRelation(new ArrayList<>());

        disRel.addToRel(Pair.of("aa", "bb"));
        disRel.addToRel(Pair.of("aa", "dd"));
        disRel.addToRel(Pair.of("cc", "cc"));
        disRel.addToRel(Pair.of("dd", "dd"));

        Set<String> expected = new HashSet<>();
        expected.add("bb");
        expected.add("dd");

        assertEquals(disRel.disables("aa"), expected);
    }

    @Test
    public void disabledInputs() {

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");


        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();


        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "cc", q3, "yy");
        givenMealy.setTransition(q2, "cc", q4, "rr");
        givenMealy.setTransition(q3, "aa", q4, "yy");

        Set<String> expected = new HashSet<>();
        expected.add("cc");
        expected.add("aa");

        assertEquals(disRelReflexive.disabledInputs(q4, givenMealy), expected);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void symsOnEachPath() {

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));
        disRelReflexive.addToRel(Pair.of("dd", "dd"));

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");


        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();


        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "bb", q2, "yy");
        givenMealy.setTransition(q2, "cc", q3, "rr");
        givenMealy.setTransition(q3, "dd", q4, "yy");

        Set<String> expected = new HashSet<>();
        expected.add("dd");
        expected.add("cc");

        assertEquals(disRelReflexive.symbolsOnEachPath(q4, givenMealy), expected);

    }

    @Test
    public void setReflexive() {

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);
        disRelReflexive.setReflexive(givenMealy);

        assertTrue(disRelReflexive.isReflexive(testInputs));
    }
}