package mealy;

import equivalence.TraceEquivalence;
import net.automatalib.automata.transducers.impl.FastMealy;
import net.automatalib.automata.transducers.impl.FastMealyState;
import net.automatalib.automata.transducers.impl.MealyTransition;
import net.automatalib.commons.util.Pair;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.junit.Test;
import relation.DisablingRelation;

import java.util.ArrayList;
import java.util.Objects;

import static org.junit.Assert.*;

@SuppressWarnings("ALL")
public class MealyHandlerTest {

    @Test
    public void eliminateDisabled() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();


        //the input "aa" is disabled in state q3
        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "bb", q2, "yy");
        givenMealy.setTransition(q2, "aa", q3, "xx");
        givenMealy.setTransition(q3, "cc", q4, "yy");
        givenMealy.setTransition(q3, "aa", q4, "yy");
        givenMealy.setTransition(q3, "bb", q4, "yy");
        givenMealy.setTransition(q4, "cc", q4, "yy");

        givenMealy = MealyHandler.eliminateDisabled(disRelReflexive, givenMealy);

        MealyTransition transition = givenMealy.getTransition(givenMealy.getState(2), "aa");

        assertNull(transition);
    }

    @Test
    public void deleteIsolated() {

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();


        //q3 and q4 are isolated
        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "bb", q2, "yy");
        givenMealy.setTransition(q3, "cc", q4, "yy");
        givenMealy.setTransition(q4, "dd", q3, "yy");

        givenMealy = MealyHandler.deleteIsolated(givenMealy);

        assertEquals(2, givenMealy.getStates().size());
    }

    @Test
    public void mergeFail() {

        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));
        disRelReflexive.addToRel(Pair.of("dd", "dd"));

        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");

        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();


        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "bb", q3, "yy");
        givenMealy.setTransition(q2, "cc", q4, "yy");
        givenMealy.setTransition(q3, "dd", q4, "yy");


        MealyHandler.mergeFail(givenMealy, disRelReflexive);
        boolean condition = givenMealy.getState(3) == null &&
                Objects.requireNonNull(givenMealy.getTransition(givenMealy.getState(1), "cc")).getSuccessor().getId() == 1
                && Objects.requireNonNull(givenMealy.getTransition(givenMealy.getState(2), "dd")).getSuccessor().getId() == 2;

        assertTrue(condition);

    }

    @Test
    public void mergeStates() {
        DisablingRelation disRelReflexive = new DisablingRelation(new ArrayList<>());

        disRelReflexive.addToRel(Pair.of("aa", "aa"));
        disRelReflexive.addToRel(Pair.of("bb", "bb"));
        disRelReflexive.addToRel(Pair.of("cc", "cc"));
        disRelReflexive.addToRel(Pair.of("dd", "dd"));


        Alphabet testInputs = new GrowingMapAlphabet<>();

        testInputs.add("aa");
        testInputs.add("bb");
        testInputs.add("cc");
        testInputs.add("dd");


        FastMealy<String, String> givenMealy = new FastMealy<>(testInputs);

        final FastMealyState q1 = givenMealy.addInitialState();
        final FastMealyState q2 = givenMealy.addState();
        final FastMealyState q3 = givenMealy.addState();
        final FastMealyState q4 = givenMealy.addState();
        final FastMealyState q6 = givenMealy.addState();


        givenMealy.setTransition(q1, "aa", q2, "xx");
        givenMealy.setTransition(q1, "cc", q3, "yy");
        givenMealy.setTransition(q2, "cc", q4, "rr");
        givenMealy.setTransition(q3, "cc", q4, "yy");
        givenMealy.setTransition(q3, "aa", q6, "xx");

        Alphabet expectedInputs = new GrowingMapAlphabet<>();

        expectedInputs.add("aa");
        expectedInputs.add("bb");
        expectedInputs.add("cc");
        expectedInputs.add("dd");


        FastMealy<String, String> expectedMealy = new FastMealy<>(expectedInputs);

        final FastMealyState p1 = expectedMealy.addInitialState();
        final FastMealyState p2 = expectedMealy.addState();
        final FastMealyState p3 = expectedMealy.addState();
        final FastMealyState p4 = expectedMealy.addState();
        final FastMealyState p6 = expectedMealy.addState();


        expectedMealy.setTransition(p1, "aa", p2, "xx");
        expectedMealy.setTransition(p1, "cc", p2, "yy");
        expectedMealy.setTransition(p2, "cc", p3, "rr");
        expectedMealy.setTransition(p2, "aa", p4, "xx");

        MealyHandler.mergeStates(givenMealy, givenMealy.getState(1), givenMealy.getState(2), disRelReflexive);

        assertTrue(givenMealy.getStates().size() == 5 && TraceEquivalence.checkEquivalence(givenMealy, expectedMealy, testInputs));

    }

    @Test
    public void splitState() {

        final Alphabet alphabet = new GrowingMapAlphabet();
        alphabet.add("aa");
        alphabet.add("bb");
        alphabet.add("cc");
        FastMealy<String, String> mealyToTest = new FastMealy(alphabet);

        final FastMealyState<String> q1 = mealyToTest.addInitialState();
        final FastMealyState<String> q2 = mealyToTest.addState();
        final FastMealyState<String> q3 = mealyToTest.addState();
        final FastMealyState<String> q4 = mealyToTest.addState();
        final FastMealyState<String> q5 = mealyToTest.addState();

        mealyToTest.setTransition(q1, "aa", q3, "11");
        mealyToTest.setTransition(q2, "bb", q3, "11");
        mealyToTest.setTransition(q3, "cc", q3, "33");
        mealyToTest.setTransition(q3, "bb", q4, "44");
        mealyToTest.setTransition(q5, "bb", q3, "11");

        FastMealy<String, String> expected = new FastMealy<>(alphabet);

        AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, mealyToTest, alphabet, expected);

        //q3 is split into 2 new states, loop transitions are copied
        Pair splitResult = MealyHandler.splitState(expected.getState(2), expected);
        ArrayList<FastMealyState> resultingStates = (ArrayList<FastMealyState>) splitResult.getFirst();

        assertTrue(resultingStates.size() == 2 && TraceEquivalence.checkEquivalence(mealyToTest, expected, alphabet));

    }

    @Test
    public void numberOfTransitions() {
        final Alphabet alphabet = new GrowingMapAlphabet();
        alphabet.add("aa");
        alphabet.add("bb");
        alphabet.add("cc");
        alphabet.add("tt");
        alphabet.add("ff");

        FastMealy<String, String> mealyToTest = new FastMealy(alphabet);

        final FastMealyState<String> q1 = mealyToTest.addState();
        mealyToTest.setInitialState(q1);
        final FastMealyState<String> q2 = mealyToTest.addState();
        final FastMealyState<String> q3 = mealyToTest.addState();
        final FastMealyState<String> q4 = mealyToTest.addState();
        final FastMealyState<String> q5 = mealyToTest.addState();
        final FastMealyState<String> q6 = mealyToTest.addState();

        mealyToTest.setTransition(q1, "aa", q2, "dd");
        mealyToTest.setTransition(q1, "tt", q4, "dd");
        mealyToTest.setTransition(q1, "bb", q3, "dd");
        mealyToTest.setTransition(q1, "cc", q5, "zz");
        mealyToTest.setTransition(q2, "ff", q4, "zz");
        mealyToTest.setTransition(q3, "ff", q5, "zz");
        mealyToTest.setTransition(q4, "ff", q6, "pp");
        mealyToTest.setTransition(q5, "ff", q6, "rr");

        assertEquals(MealyHandler.numberOfTransitions(mealyToTest), 8);
    }
}